+++
title = "Gallery"
draft = false
menu = "main"
description = " "
+++
![Chicago Harbor](https://s3.us-east-2.amazonaws.com/ying-ish/chicago_2293_polarr_copy.jpg)
<center>@Chicago Harbor: A pretty red liftfork</center>
<br/>
<br/>
![A group of Kayaking people](https://s3.us-east-2.amazonaws.com/ying-ish/chicago_2307_polarr_copy.jpg)
<center>@Chicago Harbor: A group of Kayaking people</center>
<br/>
<br/>
![beach-surfing](https://s3.us-east-2.amazonaws.com/ying-ish/beach-surfing.jpeg)
<center>@UCSB, the campus is just by the beach</center>
