+++
title = "about"
draft = false
menu = "main"
description = "Pretty much every thing about this site "
+++
[Why blogging?](http://www.ying-ish.com/docs/why-blogging/)

[Why blogging in English?](http://www.ying-ish.com/docs/write-in-english/)

[Where does the site's name come from?](http://www.ying-ish.com/docs/about-swimming-alone/)
